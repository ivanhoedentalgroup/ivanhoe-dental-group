At Ivanhoe Dental, our experienced staff provides quality dental care services in a comfortable and convenient location. We take pride in the personalized touch offered to every patient that walks through the door.

Address: 61 W. 144th St, Riverdale, IL 60827, USA

Phone: 708-849-8627

Website: https://www.ivanhoedental.com